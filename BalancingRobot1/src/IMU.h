#ifndef IMU_H
#define IMU_H

#include <stdint.h>

void IMU_init(void); 
void IMU_read(void); 
float get_accY(void); 
float get_accX(void); 
float get_gyroZ(void); 
double get_acc_angle(void); 


#endif
