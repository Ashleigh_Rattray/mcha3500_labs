#include "bluetooth.h"
#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_uart.h"
#include "cmsis_os2.h"
#include "uart.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

static UART_HandleTypeDef bluetooth_UART; 

static GPIO_InitTypeDef GPIOC_InitStructure; 

void bluetoothInit(void)
{
    /* Define structure */
        bluetooth_UART.Instance = UART4; 
        bluetooth_UART.Init.BaudRate = 115200; 
        bluetooth_UART.Init.WordLength = UART_WORDLENGTH_8B; 
        bluetooth_UART.Init.StopBits = UART_STOPBITS_1; 
        bluetooth_UART.Init.Parity = UART_PARITY_NONE; 
        bluetooth_UART.Init.Mode = UART_MODE_TX_RX;
        bluetooth_UART.Init.HwFlowCtl= UART_HWCONTROL_NONE; 
        bluetooth_UART.Init.OverSampling = UART_OVERSAMPLING_8; 
    /* Initialise */
        HAL_UART_MspInit(&bluetooth_UART); 
    /* Enable clock */
        __UART4_CLK_ENABLE(); 
    /* Configure pins */
        /* Enable clock */ 
            __HAL_RCC_GPIOC_CLK_ENABLE(); 
        /* Pins */
            GPIOC_InitStructure.Pin = GPIO_PIN_10|GPIO_PIN_11; 
            GPIOC_InitStructure.Mode = GPIO_MODE_AF_PP; 
            GPIOC_InitStructure.Pull = GPIO_NOPULL;
            GPIOC_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
            GPIOC_InitStructure.Alternate = GPIO_AF8_UART4; 
            HAL_GPIO_Init(GPIOC, &GPIOC_InitStructure); 
}