#include "data_logging.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "IMU.h"
#include "current_sensor.h"
#include "encoder.h"
#include "MotorDriver.h"


#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


/* Variable declarations */
 uint16_t logCount;
 float omega1_old = 0; 
 float omega2_old = 0; 
 float omega1; 
 float omega2; 
 static double angle_result; 
 static float gyroZ_result; 
 static void (*log_function)(void); 
 static osTimerId_t datalogTimerId; 
 static osTimerAttr_t datalogTimerAttr = 
 {
     .name = "datalogTimer"
 }; 

 /* Function declarations */
 static void log_pendulum(void *argument); 
 static void log_pointer(void *argument); 
 static void log_imu(void); 
 static void log_current(void); 
 static void log_encoder(void);
 static void log_TorqueRig(void); 
 static void log_SwingTest(void); 

 /* Function defintions */
 static void log_pendulum(void *argument)
 {
 /* Supress compiler warnings for unused arguments */
    UNUSED(argument); 

 /* Read the potentiometer voltage */
    float P_Voltage = pendulum_read_voltage(); 
 /* Print the sample time and potentiometer voltage to the serial terminal in the format [time],[
voltage] */
    printf("%f , %f\n",logCount*0.001,P_Voltage); 

 /* Increment log count */
    logCount = logCount+5; 

 /* Stop logging once 2 seconds is reached (Complete this once you have created the stop function
in the next step) */
    if(logCount == 2000)
    {
        logging_stop(); 
    }
 }

 void logging_init(void)
 {
 /* Initialise timer for use with pendulum data logging */
        datalogTimerId = osTimerNew(log_pointer, osTimerPeriodic, NULL, &datalogTimerAttr); 
 }

 void pend_logging_start(void)
 {
    /* Change function poninter to pendulum logging */ 
      log_function = &log_pendulum; 

   /* Reset the log counter */
      logCount = 0; 

   /* Start data logging timer at 200Hz */
      osTimerStart(datalogTimerId ,(1.0/200.00)*1000.0); 	
 }

 void logging_stop(void)
 {
 /* Stop data logging timer */
    osTimerStop(datalogTimerId); 
 }

void log_pointer(void *argument)
 {
    UNUSED(argument); 

    /* Call Function */
      (*log_function)(); 
 }

 void imu_logging_start(void)
{
   /* Change function pointer to the imu logging function (log_imu) */
      log_function = &log_imu; 

   /* Reset the log counter */
      logCount = 0; 

   /* Start data logging at 200Hz */
      osTimerStart(datalogTimerId ,(1.0/200.00)*1000.0); 	
}

static void log_imu(void)
{
   /* Read IMU */
      IMU_read(); 

   /* Get the imu angle from accelerometer readings */
      angle_result = get_acc_angle(); 

   /* Get the imu X gyro reading */
      gyroZ_result = get_gyroZ(); 

   /* Read the potentiometer voltage */
      float P_Voltage = pendulum_read_voltage(); 

   /* Print the time, accelerometer angle, gyro angular velocity and pot voltage values to the
      serial terminal in the format %f,%f,%f,%f\n */
         printf("%f, %f, %f\n", logCount*0.001,angle_result*180/M_PI, gyroZ_result*180/M_PI);

   /* Increment log count */
      logCount = logCount+5; 

   /* Stop logging once 5 seconds is reached */
      if(logCount == 5000)
    {
        logging_stop(); 
    }
}

void current_logging_start(void)
{
   /* Change function pointer to the imu logging function (log_imu) */
      log_function = &log_current; 

   /* Reset the log counter */
      logCount = 0; 

   /* Start data logging at 200Hz */
      osTimerStart(datalogTimerId ,(1.0/200.00)*1000.0); 	
}

static void log_current(void)
{
   /* Read currents */
      float currents[2]; 
      CurrentSensor_Init(); 
      CurrentSensor_Update(&currents); 
   /* Print currents */ 
      printf("Current 1: %f, Current 2: %f\n",currents[0],currents[1]);

   /* Increment log count */
      logCount = logCount+5; 

   /* Stop logging once 5 seconds is reached */
      if(logCount == 5000)
    {
        logging_stop(); 
    }
}

void encoder_logging_start(void)
{
   /* Change function pointer to the imu logging function (log_imu) */
      log_function = &log_encoder; 

   /* Reset the log counter */
      logCount = 0; 

   /* Start data logging at 200Hz */
      osTimerStart(datalogTimerId ,(1.0/200.00)*1000.0); 	
}

static void log_encoder(void)
{
   /* Read encoder values */
      encoder_init(); 
      float encoder1 = encoder_getValue1(); 
      float encoder2 = encoder_getValue2(); 

   /* Print currents */ 
      printf("Encoder 1: %f, Encoder 2: %f\n",encoder1,encoder2);

   /* Increment log count */
      logCount = logCount+5; 

   /* Stop logging once 5 seconds is reached */
      if(logCount == 5000)
    {
        logging_stop(); 
    }
}

void TorqueRig_logging_start(void)
{
    /* Change function pointer to the imu logging function (log_imu) */
    log_function = &log_TorqueRig;
    
    /* Reset the log counter */
    logCount = 0;
    
    /* Start data logging at 200Hz */
    osTimerStart(datalogTimerId ,(1.0/150.00)*1000.0);
}

static void log_TorqueRig(void)
{
   //* Define amplitude and frequency */
      float A = 3.0; 
      float freq = 1.5;  // 1 Hz

   /* Initialise */
      MotorDriver_Init(); 
      CurrentSensor_Init();
      encoder_init(); 

   /* Generate voltage */
      float time = 0.001*logCount; 
      float voltage = generate_sinewave(A,freq, time); 

   /* Send voltage motor to motors */ 
      Set_Movement(voltage); 

   /* Wait before reading currents */
      float currentValue[2]; 
      CurrentSensor_Init(); 
      CurrentSensor_Update(&currentValue); 

   /* Read encoder count */
      float encoder1 = encoder_getValue1(); 
      float encoder2 = encoder_getValue2(); 
    
    /* Print results */
      printf("%f %f %f %f %f %f\n",logCount*0.001, voltage, currentValue[0], currentValue[1],encoder1,encoder2);
    
    /* Increment log count */
    logCount = logCount+5;
    
    /* Stop logging once 5 seconds is reached */
    if(logCount == 10000)
    {
        logging_stop();
    }
}

void SwingTest_logging_start(void)
{
    /* Change function pointer to the imu logging function (log_imu) */
    log_function = &log_SwingTest;
    
    /* Reset the log counter */
    logCount = 0;
    
    /* Start data logging at 150Hz */
    osTimerStart(datalogTimerId ,(1.0/100.00)*1000.0);
}

static void log_SwingTest(void)
{
   
   float amplitude = 0.15; 
   float frequency = 1.5; // 1Hz

    /* Get current omega values */
      // conversion things 
         float counts_rev = 4*979.62; 
         float deg_pulse = counts_rev/360; 
      // encoder
         float encoder1 = encoder_getValue1(); 
         float encoder2 = -encoder_getValue2(); 
      // convert
         float angle1 = (encoder1/deg_pulse)*(M_PI/180); 
         float angle2 = (encoder2/deg_pulse)*(M_PI/180); 
      // rate of change 
         omega1 = (angle1 - omega1_old)/0.01; 
         omega2 = (angle2 - omega2_old)/0.01; 
      // put in array
         float omega[2]; 
         omega[0] = omega1; 
         omega[1] = omega2; 
      // redefine old value 
         omega1_old = angle1; 
         omega2_old = angle2; 

    /* Get the torque based velocity input */
      float voltages[2]; 
      float torque; 
      torque = generate_TorqueBased_sinewave(amplitude,frequency,logCount*0.01,&omega,&voltages); 

   /* Apply found voltages */
      setMotor1(voltages[0]); 
      setMotor2(voltages[1]); 
    
    /* Read values of interest */
      // Current
         float currentValue[2]; 
         CurrentSensor_Init(); 
         CurrentSensor_Update(&currentValue); 

    /* Print values of interest */
      printf("%f %f %f %f\n",logCount*0.01, torque*2, encoder1,encoder2);

    /* Increment log count */
    logCount = logCount+1;
    
    /* Stop logging once 5 seconds is reached */
    if(logCount == 1000)
    {
        logging_stop();
        Set_Movement(0.0); 
    }
}
