#ifndef MOTORDRIVER_H
#define MOTORDRIVER_H

#include <stdint.h>

void MotorDriver_Init(void); 
void Set_Forward(void);
void Set_Reverse(void); 
void set_Speed(float); 
void SetDirection(float); 
void Set_Movement(float); 
float generate_sinewave(float, float, float); 
float generate_TorqueBased_sinewave(float,float,float,float*,float*);
void setMotor1(float);
void setMotor2(float);

#endif
