#include "MotorDriver.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"

static GPIO_InitTypeDef Motor1_DirectionInit; 
static GPIO_InitTypeDef Motor2_DirectionInit; 
static GPIO_InitTypeDef Motor1_SpeedInit; 
static GPIO_InitTypeDef Motor2_SpeedInit; 
static TIM_HandleTypeDef TIM4_Init; 
static TIM_OC_InitTypeDef TIM4_Config1; 
static TIM_OC_InitTypeDef TIM4_Config2; 

#include <math.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

float generate_TorqueBased_sinewave(float amp,float f, float t,float *omega,float *voltage)
{
    float torque = amp*sin(2.0*M_PI*f*t); 
    float torque_use = torque/2; 

    /* MOTOR 1 */
        float Ra1 = 7.5894; // Define from electrical sysID
        float N1 = 20.4; // From motor data
        float Ki1 = 0.0083; // From sysID
        float Komega1 = 1.4461; // From sysID
    /* MOTOR 2 */
        float Ra2 = 7.3371; // Define from electrical sysID
        float N2 = 20.4; // From motor data
        float Ki2 = 0.0079; // From sysID
        float Komega2 = 1.4587; // From electrical sysID
    /* phi_dot */
        float omega_a1 = omega[0]; 
        float omega_a2 = omega[1]; // Somehow calculate this online from the encoder values???

    /* Voltages */
        voltage[0] = (Ra1*torque_use)/(N1*Ki1) + Komega1*omega_a1; 
        voltage[1] = (Ra2*torque_use)/(N2*Ki2) + Komega2*omega_a2; 

        return torque_use; 
}

void setMotor1(float Voltage1)
{
    float duty_cycle1 = Voltage1/11.1; 
    
    if(duty_cycle1 < 0)
    { 
        // Set to reverse
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET); 
        // Set corresponding speed
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,10000.0*(-duty_cycle1));
    }

    if(duty_cycle1 > 0)
    {
        // Set to forward
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET); 
        // Set corresponding 
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,10000.0*(duty_cycle1));
    }

    if(duty_cycle1 == 0)
    {
        __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,0.0);
    }
}

void setMotor2(float Voltage2)
{
    float duty_cycle2 = Voltage2/11.1; 
    
    if(duty_cycle2 < 0)
    { 
        // Set to reverse
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET); 
        // Set corresponding speed
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,10000.0*(-duty_cycle2));
    }

    if(duty_cycle2 > 0)
    {
        // Set to forward
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET); 
        // Set corresponding 
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,10000.0*(duty_cycle2));
    }

    if(duty_cycle2 == 0)
    {
        __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,0.0);
    }
}

float generate_sinewave(float amp, float f, float t)
{
    float Voltage = amp*sin(2.0*M_PI*f*t); 
    return Voltage; 
}

void Set_Forward(void)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET); 
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET); 
}

void Set_Reverse(void)
{
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET); 
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET); 
}

void Set_Movement(float Voltage)
{
    float duty_cycle = Voltage/11.1; 
    
    if(duty_cycle < 0)
    { 
        // Set to reverse
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_SET); 
        // Set corresponding speed
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,10000.0*(-duty_cycle));
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,10000.0*(-duty_cycle));
    }

    if(duty_cycle > 0)
    {
        // Set to forward
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_5, GPIO_PIN_SET); 
            HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6, GPIO_PIN_RESET); 
        // Set corresponding 
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,10000.0*(duty_cycle));
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,10000.0*(duty_cycle));
    }

    if(duty_cycle == 0)
    {
        __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,0.0);
        __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,0.0);
    }
}

void set_Speed(float SpeedSet)
{
    __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,10000.0*(SpeedSet/100.0));
    __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,10000.0*(SpeedSet/100.0));
}

void MotorDriver_Init(void)
{
    /* Start the GPIO clocks */
        __HAL_RCC_GPIOB_CLK_ENABLE(); 
        __HAL_RCC_GPIOC_CLK_ENABLE(); 
    /* Initialise the direction pin for high/low output */
        // Motor 1
            Motor1_DirectionInit.Pin = GPIO_PIN_5; 
            Motor1_DirectionInit.Mode = GPIO_MODE_OUTPUT_PP; 
            Motor1_DirectionInit.Pull = GPIO_NOPULL; 
            Motor1_DirectionInit.Speed = GPIO_SPEED_FREQ_HIGH; 
            HAL_GPIO_Init(GPIOC,&Motor1_DirectionInit);
        // Motor 2
            Motor2_DirectionInit.Pin = GPIO_PIN_6; 
            Motor2_DirectionInit.Mode = GPIO_MODE_OUTPUT_PP; 
            Motor2_DirectionInit.Pull = GPIO_NOPULL; 
            Motor2_DirectionInit.Speed = GPIO_SPEED_FREQ_HIGH; 
            HAL_GPIO_Init(GPIOC,&Motor2_DirectionInit);

    /* Initialise the speed pin for PWM output */
        // Start the timer clock 
            __HAL_RCC_TIM4_CLK_ENABLE(); 
        // Motor 1
            Motor1_SpeedInit.Pin = GPIO_PIN_7;
            Motor1_SpeedInit.Mode = GPIO_MODE_AF_PP;
            Motor1_SpeedInit.Pull = GPIO_NOPULL;
            Motor1_SpeedInit.Speed = GPIO_SPEED_FREQ_HIGH;
            Motor1_SpeedInit.Alternate = GPIO_AF2_TIM4;
            HAL_GPIO_Init(GPIOB,&Motor1_SpeedInit); 
        // Motor 2
            Motor2_SpeedInit.Pin = GPIO_PIN_8;
            Motor2_SpeedInit.Mode = GPIO_MODE_AF_PP;
            Motor2_SpeedInit.Pull = GPIO_NOPULL;
            Motor2_SpeedInit.Speed = GPIO_SPEED_FREQ_HIGH;
            Motor2_SpeedInit.Alternate = GPIO_AF2_TIM4;
            HAL_GPIO_Init(GPIOB,&Motor2_SpeedInit); 
        // Initialise the timer 
            TIM4_Init.Instance = TIM4;
            TIM4_Init.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1; 
            TIM4_Init.Init.CounterMode = TIM_COUNTERMODE_UP; 
            TIM4_Init.Init.Period = 10000; // this is Frequency_CPU/Desired_Frequency
            TIM4_Init.Init.ClockDivision = 0; 
            HAL_TIM_PWM_Init(&TIM4_Init); 
        // Configure Timer Channels 
            // Motor 1
                TIM4_Config1.OCMode = TIM_OCMODE_PWM1; 
                TIM4_Config1.Pulse = 0; 
                TIM4_Config1.OCPolarity = TIM_OCPOLARITY_HIGH; 
                TIM4_Config1.OCFastMode = TIM_OCFAST_DISABLE; 
                HAL_TIM_PWM_ConfigChannel(&TIM4_Init,&TIM4_Config1,TIM_CHANNEL_2); 
            // Motor 2 
                TIM4_Config2.OCMode = TIM_OCMODE_PWM1; 
                TIM4_Config2.Pulse = 0; 
                TIM4_Config2.OCPolarity = TIM_OCPOLARITY_HIGH; 
                TIM4_Config2.OCFastMode = TIM_OCFAST_DISABLE; 
                HAL_TIM_PWM_ConfigChannel(&TIM4_Init,&TIM4_Config2,TIM_CHANNEL_3); 
        // Set initial compare value to standby 
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_2,10000.0*(0.0/100.0));
            __HAL_TIM_SET_COMPARE(&TIM4_Init,TIM_CHANNEL_3,10000.0*(0.0/100.0));
        // Start the timer
            HAL_TIM_PWM_Start(&TIM4_Init,TIM_CHANNEL_2); 
            HAL_TIM_PWM_Start(&TIM4_Init,TIM_CHANNEL_3); 

}
