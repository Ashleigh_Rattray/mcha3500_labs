#include "IMUFilter.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */

 /* Define filter matrix values */
    static float filt_mK_f32[3*2] =
    {
        /* Steady state kalman gain */
             0.0025,    1.0399,
             0.0491,    0.0352,
            -0.0025,   -0.0404

    };

    static float filt_xhatold_f32[3*1] =
    {
        /* estimate of x_hat */
            0.0,
            0.0,
            0.0
    };
    static float filt_xhatnew_f32[3*1] =
    {
        /* where results will be stored */
            0.0,
            0.0,
            0.0
    };
    static float filt_Ad_f32[3*3] =
    {
        /* Discrete time A matrix */
            1.0000,    0.0,        0.0,
            0.0100,    1.0000,     0.0,
            0.0,       0.0,        1.0000
    };
    static float filt_C_f32[2*3] =
    {
        /* Continuous time C matrix */
            0.0,     1.0,     0.0,
            1.0,     0.0,     1.0
    };
    static float filt_y_f32[2*1] =
    {
        /* y matrix */
            0.0,
            0.0
    };
    static float filt_Adx_f32[3*1] =
    {
        /* Ad*xhat_old matrix */
            0.0,
            0.0,
            0.0
    };
    static float filt_Cx_f32[2*1] =
    {
        /* C*xhat_old matrix */
            0.0,
            0.0
    };
    static float filt_Kssy_f32[3*1] =
    {
        /* Kss*(y - Cxhatold) matrix */
            0.0,
            0.0,
            0.0
    };

 /* Define control matrix variables */
 // rows, columns, data array
    arm_matrix_instance_f32 filt_mK = {3, 2, (float32_t *)filt_mK_f32};
    arm_matrix_instance_f32 filt_xhatold = {3, 1, (float32_t *)filt_xhatold_f32};
    arm_matrix_instance_f32 filt_xhatnew = {3, 1, (float32_t *)filt_xhatnew_f32};
    arm_matrix_instance_f32 filt_Ad = {3, 3, (float32_t *)filt_Ad_f32};
    arm_matrix_instance_f32 filt_C = {2, 3, (float32_t *)filt_C_f32};
    arm_matrix_instance_f32 filt_y = {2, 1, (float32_t *)filt_y_f32};
    arm_matrix_instance_f32 filt_Adx = {3, 1, (float32_t *)filt_Adx_f32};
    arm_matrix_instance_f32 filt_Cx = {2, 1, (float32_t *)filt_Cx_f32};
    arm_matrix_instance_f32 filt_Kssy = {3, 1, (float32_t *)filt_Kssy_f32};

 /* Control functions */
 void filt_init(void)
 {
    arm_mat_init_f32(&filt_mK, 3, 2, (float32_t *)filt_mK_f32);
    arm_mat_init_f32(&filt_xhatold, 3, 1, (float32_t *)filt_xhatold_f32);
    arm_mat_init_f32(&filt_xhatnew, 3, 1, (float32_t *)filt_xhatnew_f32);
    arm_mat_init_f32(&filt_Ad, 3, 3, (float32_t *)filt_Ad_f32);
    arm_mat_init_f32(&filt_C, 2, 3, (float32_t *)filt_C_f32);
    arm_mat_init_f32(&filt_y, 2, 1, (float32_t *)filt_y_f32);
    arm_mat_init_f32(&filt_Adx, 3, 1, (float32_t *)filt_Adx_f32);
    arm_mat_init_f32(&filt_Cx, 2, 1, (float32_t *)filt_Cx_f32);
    arm_mat_init_f32(&filt_Kssy, 3, 1, (float32_t *)filt_Kssy_f32);
}

/* Update state vector elements */
    void filt_set_x1(float x1)
    {
        // Update state x1
            filt_xhatold_f32[0] = x1;
    }

    void filt_set_x2(float x2)
    {
        // Update state x2
            filt_xhatold_f32[1] = x2;
    }

    void filt_set_x3(float x3)
    {
        // Update state x3
            filt_xhatold_f32[2] = x3;
    }

    void filt_set_y1(float y1)
    {
        // Update state x4
            filt_y_f32[0] = y1;
    }

    void filt_set_y2(float y2)
    {
        // Update state x4
            filt_y_f32[1] = y2;
    }

/* Get the current control output */
    void getUpdate(float *xhat_new)
    {
        /* Assign new values */
            xhat_new[0] = filt_xhatnew_f32[0];
            xhat_new[1] = filt_xhatnew_f32[1];
            xhat_new[2] = filt_xhatnew_f32[2];
        
    }

 /* Update control output */
    void filt_update(void)
    {
        // Compute Ad*xhat_old
            arm_mat_mult_f32(&filt_Ad, &filt_xhatold, &filt_Adx);
        // Compute Cxhat_old
            arm_mat_mult_f32(&filt_C, &filt_xhatold, &filt_Cx);
        // Compute y(i) - Cxhat_old 
            arm_mat_sub_f32(&filt_y, &filt_Cx, &filt_Cx);
        // Compute Kss*(y(i)-Cxhat_old)
            arm_mat_mult_f32(&filt_mK, &filt_Cx, &filt_Kssy);
        // Compute new state value 
            arm_mat_add_f32(&filt_Adx, &filt_Kssy, &filt_xhatnew);
 }