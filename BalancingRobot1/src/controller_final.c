#include "controller_final.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */

 /* Define control matrix values */
    static float ctrl_mK_f32[CTRL_N_INPUT*CTRL_N_STATE] =
    {
        /*  K, 1x5 */
        -4.9880, -122.2828,  -4.0887,  -14.4052,  -1.2258
    };

    static float ctrl_x_f32[CTRL_N_STATE*1] =
    {
        /* estimate of state, 5x1 */
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
    };

    static float ctrl_u_f32[CTRL_N_INPUT*1] =
    {
        /* control action, 1x1 */
        0.0,
    };

    static float ctrl_Az_f32[1*CTRL_N_STATE] =
    {
        /* State transition matrix */
        0.0067, 0.0, 0.0, 0.0, 1.0,
    };

    static float ctrl_Bz_f32[1*1] =
    {
        /* State transition matrix */
        0.0
    };

    static float ctrl_z_f32[CTRL_N_INPUT*1] =
    {
        /* Integrator state */
        0.0,
    };

    static float ctrl_Nx_f32[4*1] =
    {
        /* Feedforward Nx */
        1.0,
        0.0,
        0.0,
        0.0
    };

    static float ctrl_Nu_f32[1*1] =
    {
        /* Feedforward Nu */
        0.0
    };

    static float ctrl_yref_f32[1*1] =
    {
        /* Feedforward yref */
        1.0
    };

    static float ctrl_xstar_f32[4*1] =
    {
        /* Feedforward x_star */
        0.0,
        0.0,
        0.0,
        0.0 
    };

    static float ctrl_ustar_f32[1*1] =
    {
        /* Feedforward u_star */
        0.0
    };

    static float ctrl_xstate_f32[4*1] =
    {
        /* x State vector defined by [x - xstar] */
        0.0,
        0.0,
        0.0,
        0.0
    };

    static float ctrl_state_f32[5*1] =
    {
        /* Full state vector defined by [x - xstar];[z] */
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    };

    static float ctrl_kstate_f32[1*1] =
    {
        /* Control term K*state */
        0.0
        
    };

    static float ctrl_Azstate_f32[1*1] =
    {
        /* Integrator update term Az*state */
        0.0
        
    };

    static float ctrl_Bzu_f32[1*1] =
    {
        /* Integrator update term Bz*u */
        
    };

 /* Define control matrix variables */
 // rows, columns, data array
    arm_matrix_instance_f32 ctrl_mK = {CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32};
    arm_matrix_instance_f32 ctrl_x = {CTRL_N_STATE, 1, (float32_t *)ctrl_x_f32};
    arm_matrix_instance_f32 ctrl_u = {CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32};
    arm_matrix_instance_f32 ctrl_Az = {1, CTRL_N_STATE, (float32_t *)ctrl_Az_f32};
    arm_matrix_instance_f32 ctrl_Bz = {1, 1, (float32_t *)ctrl_Bz_f32};
    arm_matrix_instance_f32 ctrl_z = {1, 1, (float32_t *)ctrl_z_f32};
    arm_matrix_instance_f32 ctrl_Nx = {4, 1, (float32_t *)ctrl_Nx_f32};
    arm_matrix_instance_f32 ctrl_Nu = {1, 1, (float32_t *)ctrl_Nu_f32};
    arm_matrix_instance_f32 ctrl_yref = {1, 1, (float32_t *)ctrl_yref_f32};
    arm_matrix_instance_f32 ctrl_xstar = {4, 1, (float32_t *)ctrl_xstar_f32};
    arm_matrix_instance_f32 ctrl_ustar = {1, 1, (float32_t *)ctrl_ustar_f32};
    arm_matrix_instance_f32 ctrl_xstate = {4, 1, (float32_t *)ctrl_xstate_f32};
    arm_matrix_instance_f32 ctrl_state = {5, 1, (float32_t *)ctrl_state_f32};
    arm_matrix_instance_f32 ctrl_kstate = {1, 1, (float32_t *)ctrl_kstate_f32};
    arm_matrix_instance_f32 ctrl_Azstate = {1, 1, (float32_t *)ctrl_Azstate_f32};
    arm_matrix_instance_f32 ctrl_Bzu = {1, 1, (float32_t *)ctrl_Bzu_f32};

 /* Control functions */
 void ctrl_init(void)
 {
    arm_mat_init_f32(&ctrl_mK, CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32);
    arm_mat_init_f32(&ctrl_x, CTRL_N_STATE, 1, (float32_t *)ctrl_x_f32);
    arm_mat_init_f32(&ctrl_u, CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32);
    arm_mat_init_f32(&ctrl_Az, 1, CTRL_N_STATE, (float32_t *)ctrl_Az_f32);
    arm_mat_init_f32(&ctrl_Bz, 1, 1, (float32_t *)ctrl_Bz_f32);
    arm_mat_init_f32(&ctrl_z, 1, 1, (float32_t *)ctrl_z_f32);
    arm_mat_init_f32(&ctrl_Nx, 4, 1, (float32_t *)ctrl_Nx_f32);
    arm_mat_init_f32(&ctrl_Nu, 1, 1, (float32_t *)ctrl_Nu_f32);
    arm_mat_init_f32(&ctrl_yref, 1, 1, (float32_t *)ctrl_yref_f32);
    arm_mat_init_f32(&ctrl_xstar, 4, 1, (float32_t *)ctrl_xstar_f32);
    arm_mat_init_f32(&ctrl_ustar, 1, 1, (float32_t *)ctrl_ustar_f32);
    arm_mat_init_f32(&ctrl_xstate, 4, 1, (float32_t *)ctrl_xstate_f32);
    arm_mat_init_f32(&ctrl_state, 5, 1, (float32_t *)ctrl_state_f32);
    arm_mat_init_f32(&ctrl_kstate, 1, 1, (float32_t *)ctrl_kstate_f32);
    arm_mat_init_f32(&ctrl_Azstate, 1, 1, (float32_t *)ctrl_Azstate_f32);
    arm_mat_init_f32(&ctrl_Bzu, 1, 1, (float32_t *)ctrl_Bzu_f32);
}

/* Update state vector elements */
    void ctrl_set_x1(float x1)
    {
        // Update state x1
            ctrl_x_f32[0] = x1;
    }

    void ctrl_set_x2(float x2)
    {
        // Update state x2
            ctrl_x_f32[1] = x2;
    }

    void ctrl_set_x3(float x3)
    {
        // Update state x3
            ctrl_x_f32[2] = x3;
    }

    void ctrl_set_x4(float x4)
    {
        // Update state x4
            ctrl_x_f32[3] = x4;
    }

/* Get the current control output */
    float getControl(void)
    {
        return ctrl_u_f32[0];
    }

 /* Update control output */
    void ctrl_update(void)
    {
        // Compute feedforward star values
             arm_mat_mult_f32(&ctrl_Nx, &ctrl_yref, &ctrl_xstar);
             arm_mat_mult_f32(&ctrl_Nu, &ctrl_yref, &ctrl_ustar);
        // Correct x part of state vector
            ctrl_x_f32[0] = ctrl_x_f32[0] - ctrl_xstar_f32[0]; 
            ctrl_x_f32[1] = ctrl_x_f32[1] - ctrl_xstar_f32[1]; 
            ctrl_x_f32[2] = ctrl_x_f32[2] - ctrl_xstar_f32[2]; 
            ctrl_x_f32[3] = ctrl_x_f32[3] - ctrl_xstar_f32[3]; 
        // Compute control signal
            arm_mat_mult_f32(&ctrl_mK, &ctrl_x, &ctrl_kstate);
            arm_mat_sub_f32(&ctrl_ustar, &ctrl_kstate, &ctrl_u);
        // Update integrator state
            arm_mat_mult_f32(&ctrl_Az, &ctrl_x, &ctrl_Azstate);
            arm_mat_mult_f32(&ctrl_Bz, &ctrl_u, &ctrl_Bzu);
            arm_mat_add_f32(&ctrl_Azstate, &ctrl_Bzu, &ctrl_z);
        // Put updated value where it needs to be
            ctrl_x_f32[4] = ctrl_z_f32[0];
 }
