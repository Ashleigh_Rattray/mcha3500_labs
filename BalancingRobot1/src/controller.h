#ifndef CONTROLLER_H
#define CONTROLLER_H

enum {
    CTRL_N_INPUT = 1, // Number of controller inputs
    CTRL_N_STATE = 5, // Number of states 
    CTRL_N_OUTPUT = 1, // Number of controller outputs
}; 

void ctrl_init(void); 
void ctrl_set_x1(float);
void ctrl_set_x2(float);
void ctrl_set_x3(float);
void ctrl_set_x4(float);
float getControl(void);
void ctrl_update(void); 

#endif
