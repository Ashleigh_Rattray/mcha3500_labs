#include "current_sensor.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "MotorDriver.h"
#include "data_logging.h"

static GPIO_InitTypeDef GPIOA_InitStructure; 
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;
static ADC_ChannelConfTypeDef sConfigADC2;
static float output; 
static uint32_t Timeout = 0xff;
static uint32_t Sensor1_Reading = 0; 
static uint32_t Sensor2_Reading = 0; 
static float V1 = 0; 
static float V2 = 0; 
static float Vsum1 = 0; 
static float Vsum2 = 0; 
static float counter = 0; 
static osTimerId_t offsetTimerId; 
 static osTimerAttr_t offsetTimerAttr = 
 {
     .name = "offsetTimer"
 }; 

void offsets(float *Voffset)
{
    /* Get offset 1 */
        Voffset[0] = getVsum1()/20; 
    /* Get offset 2 */
        Voffset[1] = getVsum2()/20; 
}

float getVsum1(void)
{
    return Vsum1; 
}

float getVsum2(void)
{
    return Vsum2; 
}

void currentSensorOffset(void)
{
    /* Set motor voltage to 0 V (i.e. current = 0) */
        Set_Movement(0.0); 
    /* Initialise Polling timer */
        offsetTimerId = osTimerNew(CurrentSensor_UpdateOffset, osTimerPeriodic, NULL, &offsetTimerAttr); 
    /* Do some polling for the current sensor at 0V */
        osTimerStart(offsetTimerId ,(1.0/150.00)*1000.0); 	
}

void CurrentSensor_UpdateOffset(void)
{
    /* Poll the ADC */
        // Start the ADC
            HAL_ADC_Start(&hadc1);
        // Poll for current sensor 1 (left motor)
            HAL_ADC_PollForConversion(&hadc1,Timeout);
            // Convert output
                Sensor1_Reading = HAL_ADC_GetValue(&hadc1);
                V1 = ((3.3)/4095.0)*Sensor1_Reading; 
            // Convert to respective current
                Vsum1 = Vsum1 + V1;  
        // Poll for current sensor 1 (left motor)
            HAL_ADC_PollForConversion(&hadc1,Timeout);
            // Convert output
                Sensor2_Reading = HAL_ADC_GetValue(&hadc1);
                V2 = ((3.3)/4095.0)*Sensor2_Reading; 
            // Convert to respective current
                Vsum2 = Vsum2 + V2; 

    /* Increment the sum */
        counter = counter + 1;  

    /* Check for end condition */ 
        if(counter >= 20.0)
        {
            osTimerStop(offsetTimerId); 
        }
}

void CurrentSensor_Update(float *current)
{
    /* Get offset values */
        float offset[2]; 
        offsets(&offset); 
    /* Poll the ADC */
        // Start the ADC
            HAL_ADC_Start(&hadc1);
        // Poll for current sensor 1 (left motor)
            HAL_ADC_PollForConversion(&hadc1,Timeout);
            // Convert output
                Sensor1_Reading = HAL_ADC_GetValue(&hadc1);
                V1 = ((3.3)/4095.0)*Sensor1_Reading; 
            // Convert to respective current
                current[0] = (V1 - offset[0])/0.4; 
                //current[0]=V1; 
        // Poll for current sensor 1 (left motor)
            HAL_ADC_PollForConversion(&hadc1,Timeout);
            // Convert output
                Sensor2_Reading = HAL_ADC_GetValue(&hadc1);
                V2 = ((3.3)/4095.0)*Sensor2_Reading; 
            // Convert to respective current
                current[1] = (V2 - offset[1])/0.4; 
                //current[1] = V2; 
                return current; 

}

void CurrentSensor_Start(void)
{
    CurrentSensor_Init(); 
}

void CurrentSensor_Init(void)
{
    /* Configure the ADC instance */
        hadc1.Instance = ADC1; 
        hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2; 
        hadc1.Init.Resolution = ADC_RESOLUTION_12B; 
        hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT; 
        hadc1.Init.ScanConvMode = ENABLE;
        hadc1.Init.ContinuousConvMode = DISABLE;
        hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV; 
        hadc1.Init.NbrOfConversion = 2;  
    /* Configure input channels */
        // PA0 for current sensor 1 (left motor)
            sConfigADC.Channel = ADC_CHANNEL_0; 
            sConfigADC.Rank = 1; 
            sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
            sConfigADC.Offset = 0; 
        // PA1 for current sensor 2 (right motor)
            sConfigADC2.Channel = ADC_CHANNEL_1;
            sConfigADC2.Rank = 2; 
            sConfigADC2.SamplingTime = ADC_SAMPLETIME_480CYCLES;
            sConfigADC2.Offset = 0;  
    /* Finish initialisation */
        HAL_ADC_Init(&hadc1);
        HAL_ADC_ConfigChannel(&hadc1,&sConfigADC);
        HAL_ADC_ConfigChannel(&hadc1,&sConfigADC2); 
}

void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
    /* Start the ADC clock */
        __HAL_RCC_ADC1_CLK_ENABLE(); 
    /* Start the GPIOA clock */
        __HAL_RCC_GPIOA_CLK_ENABLE(); 
    /* Initialise the current sensor pins in analog mode */ 
        GPIOA_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1; 
        GPIOA_InitStructure.Mode = GPIO_MODE_ANALOG; 
        GPIOA_InitStructure.Pull = GPIO_NOPULL; 
        GPIOA_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(GPIOA, &GPIOA_InitStructure); 
}