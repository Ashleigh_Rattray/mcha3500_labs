#ifndef ENCODER_H
#define ENCODER_H

#include <stdint.h>

void encoder_init(void); 
void EXTI1_IRQHandler(void); 
void EXTI0_IRQHandler(void); 
void EXTI2_IRQHandler(void); 
void EXTI3_IRQHandler(void); 
float encoder_getValue1(void); 
float encoder_getAngle1(float);
float encoder_getValue2(void); 
float encoder_getAngle2(float);

#endif
