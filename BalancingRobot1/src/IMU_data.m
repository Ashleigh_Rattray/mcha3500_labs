clear all
clc 
close all

%% Load data
    load('imuData_moving1');
    
    % Unpack data
        time = imuData.time;
        accAngle = imuData.angle;
        gyroVelocity = imuData.velocity;
        voltage = imuData.voltage;
        potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
        N = length(time);
        
    % Correct angle offset
        pot_angle_init = mean(potAngle(1:200));
        acc_angle_init = mean(accAngle(1:200)); 
        potAngle = pot_angle_init*potAngle; 
        
     plot(time,accAngle,time,potAngle)
    
    % Compute the angle measurement 'noise'
        angle_noise = accAngle - potAngle; 

%% Compute sample mean and variance

    gyro_sum = 0; 
    % Compute sample mean
        for i = 1:N
            gyro_sum = gyro_sum + gyroVelocity(i); 
        end 
        
        sample_mean = gyro_sum/N; 
    
    % Compute sample variance. Use Bessel's correction
        sum = 0; 
        for i = 1:N
            sum = sum + (gyroVelocity(i).' - sample_mean)*(gyroVelocity(i).' - sample_mean).'; 
        end 
        sigma = sum/(N-1); 
       
