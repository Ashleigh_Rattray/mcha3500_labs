#include "encoder.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"

static float counter = 0; 
static float counter2 = 0; 

void encoder_init(void)
{
    /* Enable the GPIO clock corresponding to the motor encoder pins */
        __HAL_RCC_GPIOC_CLK_ENABLE(); 
    /* Initialise all the respective pins in interupt mode */
        // PC0 and PC1 = Motor 1, PC2 and PC3 = Motor 2
        static GPIO_InitTypeDef Encoder_Init;
        Encoder_Init.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3; 
        Encoder_Init.Mode =  GPIO_MODE_IT_RISING_FALLING; 
        Encoder_Init.Pull = GPIO_NOPULL; 
        Encoder_Init.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(GPIOC,&Encoder_Init); 
    /* Set priorities on the external interupt lines */
        // Motor 1: PC0 and PC1
            HAL_NVIC_SetPriority(EXTI0_IRQn,0x0f,0x0f); 
            HAL_NVIC_SetPriority(EXTI1_IRQn,0x0f,0x0f); 
        // Motor 2: PC2 and PC3
            HAL_NVIC_SetPriority(EXTI2_IRQn,0x0f,0x0f); 
            HAL_NVIC_SetPriority(EXTI3_IRQn,0x0f,0x0f); 

    /* Enable the external interupts */
        // Motor 1: PC0 and PC1 
            HAL_NVIC_EnableIRQ(EXTI0_IRQn); 
            HAL_NVIC_EnableIRQ(EXTI1_IRQn); 
        // Motor 2: PC2 and PC3
            HAL_NVIC_EnableIRQ(EXTI2_IRQn); 
            HAL_NVIC_EnableIRQ(EXTI3_IRQn); 
}

/* PC0 */
void EXTI0_IRQHandler(void)
 {
     /* Check if PC0 == PC1. Adjust encoder count accordingly. */
     if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)){
        counter++; 
     }
     else {
        counter--; 
     }

     /* Reset interrupt */
        HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0); 
}

/* PC1 */
void EXTI1_IRQHandler(void)
{
    /* Check if PC0 == PC1. Adjust encoder count accordingly. */
        if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)){
            counter--; 
        }
        else {
            counter++; 
        }

    /* Reset interrupt */
        HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1); 
}

/* PC2 */
void EXTI2_IRQHandler(void)
 {
     /* Check if PC0 == PC1. Adjust encoder count accordingly. */
     if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_2) == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_3)){
        counter2++; 
     }
     else {
        counter2--; 
     }

     /* Reset interrupt */
        HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2); 
}


/* PC3 */
void EXTI3_IRQHandler(void)
{
    /* Check if PC0 == PC1. Adjust encoder count accordingly. */
        if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_2) == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_3)){
            counter2--; 
        }
        else {
            counter2++; 
        }

    /* Reset interrupt */
        HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3); 
}

/* Get the encoder count */
float encoder_getValue1(void)
{
    return counter;
}

float encoder_getValue2(void)
{
    return counter2;
}

/* Get Angle */
float encoder_getAngle1(float counter)
{
    /* Convert to angle (in radians) */
        float angle1 = counter/155.9114; 
    /* Return the value */
        return angle1;
}

float encoder_getAngle2(float counter)
{
    /* Convert to angle (in radians) */
        float angle2 = counter/155.9114; 
    /* Return the value */
        return angle2;
}

