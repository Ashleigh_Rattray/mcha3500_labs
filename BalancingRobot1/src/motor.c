#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "motor.h"

static GPIO_InitTypeDef GPIOA_Init;
static TIM_HandleTypeDef TIM3_Init; 
static TIM_OC_InitTypeDef TIM3_Config; 
static int32_t counter = 0; 

void motor_PWM_init(void)
 {
     /* Enable TIM3 clock */
        __HAL_RCC_TIM3_CLK_ENABLE(); 
     /* Enable GPIOA clock */
        __HAL_RCC_GPIOA_CLK_ENABLE(); 
    
     /* Initialise PA6 with:
            - Pin 6
            - Alternate function push-pull mode
            - No pull
            - High frequency
            - Alternate function 2 - Timer 3*/
                GPIOA_Init.Pin = GPIO_PIN_6; 
                GPIOA_Init.Mode =  GPIO_MODE_AF_PP; 
                GPIOA_Init.Pull = GPIO_NOPULL; 
                GPIOA_Init.Speed = GPIO_SPEED_FREQ_HIGH;
                GPIOA_Init.Alternate = GPIO_AF2_TIM3; 
                HAL_GPIO_Init(GPIOA,&GPIOA_Init); 
    
     /* Initialise timer 3 with:
            - Instance TIM3
            - Prescaler of 1
            - Counter mode up
            - Timer period to generate a 10kHz signal
            - Clock division of 0 */
                TIM3_Init.Instance = TIM3;
                TIM3_Init.Init.Prescaler = TIM_CLOCKPRESCALER_DIV1; 
                TIM3_Init.Init.CounterMode = TIM_COUNTERMODE_UP; 
                TIM3_Init.Init.Period = 10000; // this is Frequency_CPU/Desired_Frequency
                TIM3_Init.Init.ClockDivision = 0; 
                HAL_TIM_PWM_Init(&TIM3_Init); 
    
     /* Configure timer 3, channel 1 with:
            - Output compare mode PWM1
            - Pulse = 0
            - OC polarity high
            - Fast mode disabled */
                TIM3_Config.OCMode = TIM_OCMODE_PWM1; 
                TIM3_Config.Pulse = 0; 
                TIM3_Config.OCPolarity = TIM_OCPOLARITY_HIGH; 
                TIM3_Config.OCFastMode = TIM_OCFAST_DISABLE; 
                HAL_TIM_PWM_ConfigChannel(&TIM3_Init,&TIM3_Config,TIM_CHANNEL_1); 
    
     /* Set initial Timer 3, channel 1 compare value */
            __HAL_TIM_SET_COMPARE(&TIM3_Init,TIM_CHANNEL_1,10000.0*(25.0/100.0)); // 25% duty cycle
    
     /* Start Timer 3, channel 1 */
            HAL_TIM_PWM_Start(&TIM3_Init,TIM_CHANNEL_1); 
 }

// void motor_encoder_init(void)
//  {
//     /* Enable GPIOC clock */
//         __HAL_RCC_GPIOC_CLK_ENABLE(); 

//     /* Initialise PC0, PC1 with:
//             - Pin 0|1
//             - Interrupt rising and falling edge
//             - No pull
//             - High frequency */
//                 static GPIO_InitTypeDef GPIOC_Init;
//                 GPIOC_Init.Pin = GPIO_PIN_0|GPIO_PIN_1; 
//                 GPIOC_Init.Mode =  GPIO_MODE_IT_RISING_FALLING; 
//                 GPIOC_Init.Pull = GPIO_NOPULL; 
//                 GPIOC_Init.Speed = GPIO_SPEED_FREQ_HIGH;
//                 //GPIOC_Init.Alternate = ; 
//                 HAL_GPIO_Init(GPIOC,&GPIOC_Init); 

//     /* Set priority of external interrupt lines 0,1 to 0x0f, 0x0f
//     To find the IRQn_Type definition see "MCHA3500 Windows Toolchain\workspace\STM32Cube_F4_FW\Drivers\
//     CMSIS\Device\ST\STM32F4xx\Include\stm32f446xx.h" */
//         HAL_NVIC_SetPriority(EXTI0_IRQn,0x0f,0x0f); 
//         HAL_NVIC_SetPriority(EXTI1_IRQn,0x0f,0x0f); 
    
//     /* Enable external interrupt for lines 0, 1 */
//         HAL_NVIC_EnableIRQ(EXTI0_IRQn); 
//         HAL_NVIC_EnableIRQ(EXTI1_IRQn); 
//  }

//  void EXTI0_IRQHandler(void)
//  {
//      /* Check if PC0 == PC1. Adjust encoder count accordingly. */
//      if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)){
//         counter++; 
//      }
//      else {
//         counter--; 
//      }

//      /* Reset interrupt */
//         HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0); 
// }

// void EXTI1_IRQHandler(void)
// {
//     /* Check if PC0 == PC1. Adjust encoder count accordingly. */
//         if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1)){
//             counter--; 
//         }
//         else {
//             counter++; 
//         }

//     /* Reset interrupt */
//         HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1); 
// }

// int32_t motor_encoder_getValue(void)
// {
//  return counter;
// }



