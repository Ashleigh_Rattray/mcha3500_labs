#ifndef CURRENT_SENSOR_H
#define CURRENT_SENSOR_H

#include <stdint.h>

void CurrentSensor_Update(float*); 
void CurrentSensor_Start(void); 
void CurrentSensor_Init(void); 
void currentSensorOffset(void); 
void CurrentSensor_UpdateOffset(void); 
float getVsum1(void);
float getVsum2(void);
void offsets(float*); 

#endif
