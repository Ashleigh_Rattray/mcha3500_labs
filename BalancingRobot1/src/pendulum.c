#include <stdint.h>
#include "stm32f4xx_hal.h"

#include "pendulum.h"
//#include "stm32f4xx hal.h" 
//#include "cmsis os2.h" 
static GPIO_InitTypeDef GPIOB_Init;
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfig1; 
static uint32_t output = 0; 
static float Pendulum_Voltage = 0; 

void pendulum_init(void)
 {
 /* Enable ADC1 clock */
    __HAL_RCC_ADC1_CLK_ENABLE(); 
 /* Enable GPIOB clock */
    __HAL_RCC_GPIOB_CLK_ENABLE(); 

 /* Initialise PB0 with:
        - Pin 0
        - Analog mode
        - No pull
        - High frequency */
            GPIOB_Init.Pin = GPIO_PIN_0; 
            GPIOB_Init.Mode = GPIO_MODE_ANALOG; 
            GPIOB_Init.Pull = GPIO_NOPULL; 
            GPIOB_Init.Speed = GPIO_SPEED_FREQ_HIGH;
            // GPIOB_Init.Alternate = ; 
            HAL_GPIO_Init(GPIOB,&GPIOB_Init); 

 /* Initialise ADC1 with:
        - Instance ADC 1
        - Div 2 prescaler
        - 12 bit resolution
        - Data align right
        - Continuous conversion mode disabled
        - Number of conversions = 1
 Note: Configuration parameters not mentioned above
 are not required for this lab. */
            hadc1.Instance = ADC1; 
            hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2; 
            hadc1.Init.Resolution = ADC_RESOLUTION_12B; 
            hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT; 
            hadc1.Init.ContinuousConvMode = DISABLE;
            hadc1.Init.NbrOfConversion = 1;
            HAL_ADC_Init(&hadc1); 

 /* Configure ADC channel to:
        - Channel 8
        - Rank 1
        - Sampling time 480 cycles
        - Offset 0 */
            sConfig1.Channel = ADC_CHANNEL_8; 
            sConfig1.Rank = 1; 
            sConfig1.SamplingTime = ADC_SAMPLETIME_480CYCLES; 
            sConfig1.Offset = 0; 
            HAL_ADC_ConfigChannel(&hadc1,&sConfig1); 
 }

 float pendulum_read_voltage(void)
 {
    /* Start ADC */
        HAL_ADC_Start(&hadc1); 

    /* Poll for conversion. Use timeout of 0xFF. */
        HAL_ADC_PollForConversion(&hadc1,0xFF); 

    /* Get ADC value */
        output = HAL_ADC_GetValue(&hadc1); 

    /* Stop ADC */
        HAL_ADC_Stop(&hadc1); 

    /* Compute voltage from ADC reading. Hint: 2^12-1 = 4095 */
        Pendulum_Voltage = (3.3/4095.0)*output; 

    /* Return the computed voltage */
        return Pendulum_Voltage; 

 }