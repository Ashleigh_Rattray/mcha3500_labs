#ifndef IMUFILTER_H
#define IMUFILTER_H

void filt_init(void); 
void filt_set_x1(float);
void filt_set_x2(float);
void filt_set_x3(float);
void filt_set_y1(float);
void filt_set_y2(float);
void getUpdate(float*);
void filt_update(void); 

#endif
