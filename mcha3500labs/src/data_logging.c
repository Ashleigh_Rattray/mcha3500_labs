#include "data_logging.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "IMU.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif


/* Variable declarations */
 uint16_t logCount;
 static double angle_result; 
 static float gyroZ_result; 
 static void (*log_function)(void); 
 static osTimerId_t datalogTimerId; 
 static osTimerAttr_t datalogTimerAttr = 
 {
     .name = "datalogTimer"
 }; 

 /* Function declarations */
 static void log_pendulum(void *argument); 
 static void log_pointer(void *argument); 
 static void log_imu(void); 

 /* Function defintions */
 static void log_pendulum(void *argument)
 {
 /* Supress compiler warnings for unused arguments */
    UNUSED(argument); 

 /* Read the potentiometer voltage */
    float P_Voltage = pendulum_read_voltage(); 
 /* Print the sample time and potentiometer voltage to the serial terminal in the format [time],[
voltage] */
    printf("%f , %f\n",logCount*0.001,P_Voltage); 

 /* Increment log count */
    logCount = logCount+5; 

 /* Stop logging once 2 seconds is reached (Complete this once you have created the stop function
in the next step) */
    if(logCount == 2000)
    {
        logging_stop(); 
    }
 }

 void logging_init(void)
 {
 /* Initialise timer for use with pendulum data logging */
        datalogTimerId = osTimerNew (log_pointer, osTimerPeriodic, NULL, &datalogTimerAttr); 
 }

 void pend_logging_start(void)
 {
    /* Change function poninter to pendulum logging */ 
      log_function = &log_pendulum; 

   /* Reset the log counter */
      logCount = 0; 

   /* Start data logging timer at 200Hz */
      osTimerStart(datalogTimerId ,(1.0/200.00)*1000.0); 	
   }
 }

 void logging_stop(void)
 {
 /* Stop data logging timer */
    osTimerStop(datalogTimerId); 
 }

void log_pointer(void *argument)
 {
    UNUSED(argument); 

    /* Call Function */
      (*log_function)(); 
 }

 void imu_logging_start(void)
{
   /* Change function pointer to the imu logging function (log_imu) */
      log_function = &log_imu; 

   /* Reset the log counter */
      logCount = 0; 

   /* Start data logging at 200Hz */
      osTimerStart(datalogTimerId ,(1.0/200.00)*1000.0); 	
}

static void log_imu(void)
{
   /* Read IMU */
      IMU_read(); 

   /* Get the imu angle from accelerometer readings */
      angle_result = get_acc_angle(); 

   /* Get the imu X gyro reading */
      gyroZ_result = get_gyroZ(); 

   /* Read the potentiometer voltage */
      float P_Voltage = pendulum_read_voltage(); 

   /* Print the time, accelerometer angle, gyro angular velocity and pot voltage values to the
      serial terminal in the format %f,%f,%f,%f\n */
         printf("%f, %f, %f\n", logCount*0.001,angle_result*180/M_PI, gyroZ_result*180/M_PI);

   /* Increment log count */
      logCount = logCount+5; 

   /* Stop logging once 5 seconds is reached */
      if(logCount == 5000)
    {
        logging_stop(); 
    }
}
