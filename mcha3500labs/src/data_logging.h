#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H

#include <stdint.h>
void logging_init(void); 
void pend_logging_start(void); 
void logging_stop(void); 
void imu_logging_start(void); 

#endif
