#ifndef PENDULUM_H
#define PENDULUM_H

#include <stdint.h>

void pendulum_init(void);
float pendulum_read_voltage(void); 

#endif
