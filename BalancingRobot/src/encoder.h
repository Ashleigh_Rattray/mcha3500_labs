#ifndef ENCODER_H
#define ENCODER_H

#include <stdint.h>

void encoder_init(void); 
void EXTI1_IRQHandler(void); 
void EXTI0_IRQHandler(void); 
float encoder_getValue(void); 
float encoder_getAngle(float);

#endif
