#ifndef CURRENT_SENSOR_H
#define CURRENT_SENSOR_H

#include <stdint.h>

void getCurrent(void); 
void CurrentSensor_Update(float*); 
void CurrentSensor_Start(void); 
void CurrentSensor_Init(void); 

#endif
