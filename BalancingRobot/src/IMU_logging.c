#include "IMU_logging.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "IMU.h"

/* Declare important variables */
    #ifndef M_PI
        #define M_PI 3.14159265358979323846
    #endif

    static double angle_result; 
    static float gyroZ_result; 

/* Log the IMU */
void log_imu(void)
{
   /* Read IMU */
      IMU_read(); 

   /* Get the imu angle from accelerometer readings */
      angle_result = get_acc_angle(); 

   /* Get the imu X gyro reading */
      gyroZ_result = get_gyroZ(); 

   /* Print the time, accelerometer angle, gyro angular velocity and pot voltage values to the
      serial terminal in the format %f,%f,%f,%f\n */
         printf("%f, %f\n",angle_result*180/M_PI, gyroZ_result*180/M_PI);
}


