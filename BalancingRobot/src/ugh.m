clear all 
close all 
clc 

%% Load data for gyroscope
    load('imuData_still');
    
    % Unpack data
        time = imuData.time;
        accAngle = imuData.angle;
        gyroVelocity = imuData.velocity;
        voltage = imuData.voltage;
        potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
        N = length(time);
        
    % Correct angle offset
        pot_angle_init = mean(potAngle(1:200));
        acc_angle_init = mean(accAngle(1:200));
        potAngle = potAngle - (pot_angle_init - acc_angle_init); 
        
    % Compute the angle measurement 'noise'
        angle_noise = accAngle - potAngle; 
        
%% Compute sample mean and variance (GYROSCOPE)

    gyro_sum = 0; 
    % Compute sample mean
        for i = 1:N
            gyro_sum = gyro_sum + gyroVelocity(i); 
        end 
        samplemean_gyro = gyro_sum/N; 
    
    % Compute sample variance. Use Bessel's correction
        sum = 0; 
        for i = 1:N
            sum = sum + (gyroVelocity(i).' - samplemean_gyro)*(gyroVelocity(i).' - samplemean_gyro).'; 
        end 
        sigma_gyro = sum/(N-1); 
        
%% Load data for the accelerometer angle
    load('imuData_moving2');
    
    % Unpack data
        time = imuData.time;
        accAngle = imuData.angle;
        gyroVelocity = imuData.velocity;
        voltage = imuData.voltage;
        potAngle = (voltage-2.125)*(240/3.3)*(pi/180);
        N = length(time);
        
    % Correct angle offset
        pot_angle_init = mean(potAngle(1:200));
        acc_angle_init = mean(accAngle(1:200));
        potAngle = potAngle - (pot_angle_init - acc_angle_init); 
        
    % Compute the angle measurement 'noise'
        angle_noise = accAngle - potAngle; 

%% Compute sample mean and variance for the accelerometer
        
    angle_sum = 0; 
    % Compute sample mean
        for i = 1:N
            angle_sum = angle_sum + angle_noise(i).'; 
        end 
        samplemean_angle = angle_sum/N; 
    
    % Compute sample variance. Use Bessel's correction
        sum = 0; 
        for i = 1:N
            sum = sum + (angle_noise(i).' - samplemean_angle)*(angle_noise(i).' - samplemean_angle).'; 
        end 
        sigma_acc = sum/(N-1);
        
        R = [sigma_acc 0 ; 0 sigma_gyro]; 
        
%% Set up optimisation 
    % Define the cost 
        get_angle = @(x) x(:,2); 
        get_gyro = @(x) x(:,1) + x(:,3); 
        cost = @(params) norm(potAngle - get_angle(runKF(accAngle,gyroVelocity,N,params))) + norm(gyroVelocity - get_gyro(runKF(accAngle,gyroVelocity,N,params)));
    % Run optimisation 
        params0 = [1e-4 ; 0 ; 1e-4 ; 0 ; 0 ; 1e-4]; 
        params_opt = fminunc(cost,params0);     
    % Pull out results
        L_opt = [params_opt(1) 0 0 ; params_opt(2) params_opt(3) 0 ; params_opt(4) params_opt(5) params_opt(6)]; 
        Q_opt = L_opt*transpose(L_opt); 
        
        
%% Plot 
    % Run sim 
        [x_res, p_res] = runKF(accAngle, gyroVelocity, N, params_opt); 
    
    figure(1)
    subplot(2,1,1)
    hold on 
    plot(time,accAngle,'y','LineWidth',1.5)
    plot(time, potAngle,'b','LineWidth',1.5)
    plot(time,x_res(:,2),'r','LineWidth',1.5)
    legend('Accelerometer angle','Potentiometer Angle','Kalman Filter angle')
    xlabel('Time (s)')
    ylabel('Angle')
    grid on 
    hold off 
    
    subplot(2,1,2)
    hold on 
    plot(time, gyroVelocity,'b','Linewidth',1.5)
    plot(time, x_res(:,1), 'r', 'LineWidth',1.5)
    legend('Gyro Velocity','Kalman Filter Velocity')
    xlabel('Time (s)')
    ylabel('Velocity')
    grid on
    hold off
        
%% Functions
function [x_KF, P_KF] = runKF(angle_accel, velocity_gyro, N,params)
    % Unpack parameters into the Cholesky decomposition of Q
        L = [params(1) 0 0 ; params(2) params(3) 0 ; params(4) params(5) params(6)]; 
    % Compute Q (Process noise) from L
        Q = L*transpose(L); 
    % Define contimuout time model
        Ac = [0 0 0 ; 1 0 0 ; 0 0 0]; 
        Bc = [];
        C = [0 1 0 ; 1 0 1]; 
    % Discretise model
        T = 0.005; 
        [Ad, Bd] = c2d(Ac,Bc,T); 
    % Define Kalman filter initial conditions
        % Initial state estimate
            xm = [0; 0; 0];
        % Initial estimate error covariance
            Pm = 1*eye(3);
    % Measurement noise covariance
        R = [0.0502 0 ; 0 0] ; 
    % Process noise covariance
        %Q = [1e-6 0 0; 0 1e-6 0; 0 0 1e-6];
    % Allocate space to save results
        x_KF = zeros(N,3);
        P_KF = zeros(3,3,N);
    % Run Kalman Filter
        for i=1:N
            % Pack measurement vector
                yi = [angle_accel(i) ; velocity_gyro(i)];
            % Correction step
                % Compute Kalman gain
                    Kk = Pm*C.'/(C*Pm*C.'+R);
                % Compute corrected state estimate
                    xp = xm + Kk*(yi - C*xm);
                % Compute new measurement error covariance
                    Pp = (eye(3)-Kk*C)*Pm*(eye(3) - Kk*C).' + Kk*R*Kk.'; 
            % Prediction step
                % Predict next state
                    xm = Ad*xp;
                % Compute prediction error covariance
                    Pm = Ad*Pp*Ad.' + Q;
                % Store results
                    x_KF(i,:) = xp.';
                    P_KF(:,:,i) = Pm;
        end
end