#include "current_sensor.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"

static GPIO_InitTypeDef GPIOA_InitStructure; 
static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;
static ADC_ChannelConfTypeDef sConfigADC2;
static float output; 
static uint32_t Timeout = 0xff;
static uint32_t Sensor1_Reading = 0; 
static uint32_t Sensor2_Reading = 0; 
static float V1 = 0; 
static float V2 = 0; 

void getCurrent(void)
{
    float currents[2]; 
    CurrentSensor_Update(&currents); 
    printf("Current Sensor 1: %f\n Current Sensor 2: %f \n",currents[0],currents[1]); 
}

void CurrentSensor_Update(float *current)
{
    /* Poll the ADC */
        // Start the ADC
            HAL_ADC_Start(&hadc1);
        // Poll for current sensor 1 (left motor)
            HAL_ADC_PollForConversion(&hadc1,Timeout);
            // Convert output
                Sensor1_Reading = HAL_ADC_GetValue(&hadc1);
                V1 = ((3.3)/4095.0)*Sensor1_Reading; 
            // Convert to respective current
                current[0] = (V1 - 2.5)/0.4; 
        // Poll for current sensor 1 (left motor)
            HAL_ADC_PollForConversion(&hadc1,Timeout);
            // Convert output
                Sensor2_Reading = HAL_ADC_GetValue(&hadc1);
                V2 = ((3.3)/4095.0)*Sensor2_Reading; 
            // Convert to respective current
                current[1] = (V2 - 2.5)/0.4; 

                return current; 

}

void CurrentSensor_Start(void)
{
    CurrentSensor_Init(); 
}

void CurrentSensor_Init(void)
{
    /* Configure the ADC instance */
        hadc1.Instance = ADC1; 
        hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2; 
        hadc1.Init.Resolution = ADC_RESOLUTION_12B; 
        hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT; 
        hadc1.Init.ScanConvMode = ENABLE;
        hadc1.Init.ContinuousConvMode = DISABLE;
        hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV; 
        hadc1.Init.NbrOfConversion = 2;  
    /* Configure input channels */
        // PA0 for current sensor 1 (left motor)
            sConfigADC.Channel = ADC_CHANNEL_0; 
            sConfigADC.Rank = 1; 
            sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
            sConfigADC.Offset = 0; 
        // PA1 for current sensor 2 (right motor)
            sConfigADC2.Channel = ADC_CHANNEL_1;
            sConfigADC2.Rank = 2; 
            sConfigADC2.SamplingTime = ADC_SAMPLETIME_480CYCLES;
            sConfigADC2.Offset = 0;  
    /* Finish initialisation */
        HAL_ADC_Init(&hadc1);
        HAL_ADC_ConfigChannel(&hadc1,&sConfigADC);
        HAL_ADC_ConfigChannel(&hadc1,&sConfigADC2); 
}

void HAL_ADC_MspInit(ADC_HandleTypeDef *hadc)
{
    /* Start the ADC clock */
        __HAL_RCC_ADC1_CLK_ENABLE(); 
    /* Start the GPIOA clock */
        __HAL_RCC_GPIOA_CLK_ENABLE(); 
    /* Initialise the current sensor pins in analog mode */ 
        GPIOA_InitStructure.Pin = GPIO_PIN_0|GPIO_PIN_1; 
        GPIOA_InitStructure.Mode = GPIO_MODE_ANALOG; 
        GPIOA_InitStructure.Pull = GPIO_SPEED_FREQ_HIGH;
        GPIOA_InitStructure.Speed = GPIO_NOPULL;
        HAL_GPIO_Init(GPIOA, &GPIOA_InitStructure); 
}