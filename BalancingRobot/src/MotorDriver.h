#ifndef MOTORDRIVER_H
#define MOTORDRIVER_H

#include <stdint.h>

void MotorDriver_Init(void); 
void Set_Forward(void);
void Set_Reverse(void); 
void set_Speed(float); 
void SetDirection(float); 
void Set_Movement(float); 

#endif
