#include "IMU.h"

#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "math.h"

#include "tm_stm32_mpu6050.h"

/* Define pi */
    #ifndef M_PI
        #define M_PI  3.14159265358979323846
    #endif 

/* Variable declarations */
    TM_MPU6050_t IMU_datastruct;
    static float accelY_result; 
    static float accelX_result; 
    static float gyroZ_result; 
    static double angle_result; 

/* Function defintions */
void IMU_init(void)
{
    /* Initialise IMU with AD0 LOW, accelleration sensitivity +-4g, gyroscope+-250 deg/s */
        TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s); 
    
}

void IMU_read(void)
{
    /* Read all the IMU values */
        TM_MPU6050_ReadAll(&IMU_datastruct);	

}

float get_accY(void)
{
    /* Convert acceleration reading to ms^-2 */ 
        accelY_result = ((IMU_datastruct.Accelerometer_Y)/8192.0)*9.81;
    /* Return the Y accerlation */
        return accelY_result; 
}

float get_accX(void)
{
    /* Convert acceleration reading to ms^-2 */ 
        accelX_result = ((IMU_datastruct.Accelerometer_X)/8192.0)*9.81;
    /* Return the x accerlation */
        return accelX_result;

}

float get_gyroZ(void)
{
    /* Convert result to radians */
        gyroZ_result = ((IMU_datastruct.Gyroscope_Z)/131.072)*(M_PI/180.0);
    /* Return the Z angular velocity (in radians) */   
        return gyroZ_result; 
    
}

double get_acc_angle(void)
{
    /* Compute the IMU angle using accY and accZ and the function atan2 */
        angle_result = -atan2(get_accX(),get_accY()); 

    /* Return the IMU angle */
        return angle_result; 
}

